class ElectrosController < ApplicationController
  before_action :set_electro, only: [:show, :edit, :update, :destroy]
  before_filter :authentication_check, except: :index 
  layout "admin", except: :index
  # GET /electros
  # GET /electros.json
  def index
    @electros = Electro.all
  end

  # GET /electros/1
  # GET /electros/1.json
  def show
  end

  # GET /electros/new
  def new
    @electro = Electro.new
  end

  # GET /electros/1/edit
  def edit
  end

  # POST /electros
  # POST /electros.json
  def create
    @electro = Electro.new(electro_params)
    unless params[:electro][:img].nil?
      @electro.img = params[:electro][:img].original_filename
      upload_image params[:electro][:img]
    end
    respond_to do |format|
      if @electro.save
        format.html { redirect_to @electro, notice: 'Electro was successfully created.' }
        format.json { render :show, status: :created, location: @electro }
      else
        format.html { render :new }
        format.json { render json: @electro.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /electros/1
  # PATCH/PUT /electros/1.json
  def update
    respond_to do |format|
      if @electro.update(electro_params)
        format.html { redirect_to @electro, notice: 'Electro was successfully updated.' }
        format.json { render :show, status: :ok, location: @electro }
      else
        format.html { render :edit }
        format.json { render json: @electro.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /electros/1
  # DELETE /electros/1.json
  def destroy
    @electro.destroy
    respond_to do |format|
      format.html { redirect_to electros_url, notice: 'Electro was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
   #Uplaod method
    def upload_image file_put
      File.open(Rails.root.join('public', 'uploads', file_put.original_filename), 'wb') do |file|
        file.write(file_put.read)
      end
    end
    # Use callbacks to share common setup or constraints between actions.
    def set_electro
      @electro = Electro.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def electro_params
      params.require(:electro).permit(:img, :name, :about)
    end
end
