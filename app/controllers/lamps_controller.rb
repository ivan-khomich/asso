require 'set'
class Array
  def included_in? array
    result = true
    array.each do |element|
      unless self.include?(element)
        result = false
      end
    end
    result
  end
end

class LampsController < ApplicationController
  before_action :set_lamp, only: [:show, :edit, :update, :destroy]
  before_filter :authentication_check, only: [:lamps_admin]
  layout "admin", only: [:lamps_admin]
  # GET /lamps
  # GET /lamps.json
  def index
    @lamps = Lamp.all
    @brands = Brand.limit(8)
  end

  def lamps_admin
    session[:is_admin] = true
    flash[:success] = "Успех при авторизации"
  end

  # GET /lamps/1
  # GET /lamps/1.json
  def show
  end

  def brands
    @lamps = Lamp.all
  end

  def contact
  end

  def sort
    @brands = params[:price_category] == "all" ? Brand.all : Brand.where(:price_category => params[:price_category])
    @styles = JSON.parse(params[:styles])
    results = []
    unless @styles.empty?      
      @brands.each do |brand|
        brand_styles = JSON.parse(brand.styles)
        if brand_styles.included_in?(@styles)        
          results.push(brand)
        end
      end
    else
      results = @brands
    end
    render :json => results.to_json
  end
  # GET /lamps/new
  def new
    @lamp = Lamp.new
  end

  # GET /lamps/1/edit
  def edit
  end

  # POST /lamps
  # POST /lamps.json
  def create
    @lamp = Lamp.new(lamp_params)

    respond_to do |format|
      if @lamp.save
        format.html { redirect_to @lamp, notice: 'Lamp was successfully created.' }
        format.json { render :show, status: :created, location: @lamp }
      else
        format.html { render :new }
        format.json { render json: @lamp.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /lamps/1
  # PATCH/PUT /lamps/1.json
  def update
    respond_to do |format|
      if @lamp.update(lamp_params)
        format.html { redirect_to @lamp, notice: 'Lamp was successfully updated.' }
        format.json { render :show, status: :ok, location: @lamp }
      else
        format.html { render :edit }
        format.json { render json: @lamp.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /lamps/1
  # DELETE /lamps/1.json
  def destroy
    @lamp.destroy
    respond_to do |format|
      format.html { redirect_to lamps_url, notice: 'Lamp was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_lamp
      @lamp = Lamp.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def lamp_params
      params.require(:lamp).permit(:name, :picture, :type_of_production, :brand_id, :description, :cost, :availability, :novelty, :action)
    end
end
