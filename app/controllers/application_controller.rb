class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  USER, PASSWORD = 'admin', '123'

  def authentication_check
   authenticate_or_request_with_http_basic do |user, password|
    if user == USER && password == PASSWORD
    	session[:is_admin] = true
    	return true
    else
    	return false
    end
   end
  end
end
