json.array!(@products) do |product|
  json.extract! product, :id, :img, :price, :about, :size, :amount, :article, :brand, :metal_color, :glass_color
  json.url product_url(product, format: :json)
end
