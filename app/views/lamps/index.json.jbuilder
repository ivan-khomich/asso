json.array!(@lamps) do |lamp|
  json.extract! lamp, :id, :name, :picture, :type, :brand_id, :link, :prem_class, :styles
  json.url lamp_url(lamp, format: :json)
end
