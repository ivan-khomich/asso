json.array!(@brands) do |brand|
  json.extract! brand, :id, :name, :picture, :price_top, :price_bottom, :link, :styles
  json.url brand_url(brand, format: :json)
end
