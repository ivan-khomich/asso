json.array!(@projects) do |project|
  json.extract! project, :id, :img, :date, :about
  json.url project_url(project, format: :json)
end
