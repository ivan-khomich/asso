json.array!(@electros) do |electro|
  json.extract! electro, :id, :img, :name, :about
  json.url electro_url(electro, format: :json)
end
