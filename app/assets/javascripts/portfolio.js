// Jquery Browser Detection
(function() {
    "use strict";
    var matched, browser;
    jQuery.uaMatch = function( ua ) {
        ua = ua.toLowerCase();

        var match = /(chrome)[ \/]([\w.]+)/.exec( ua ) ||
            /(webkit)[ \/]([\w.]+)/.exec( ua ) ||
            /(opera)(?:.*version|)[ \/]([\w.]+)/.exec( ua ) ||
            /(msie) ([\w.]+)/.exec( ua ) ||
            ua.indexOf("compatible") < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec( ua ) ||
            [];

        return {
            browser: match[ 1 ] || "",
            version: match[ 2 ] || "0"
        };
    };
    matched = jQuery.uaMatch( navigator.userAgent );
    browser = {};
    if ( matched.browser ) {
        browser[ matched.browser ] = true;
        browser.version = matched.version;
    }
    if ( browser.chrome ) {
        browser.webkit = true;
    } else if ( browser.webkit ) {
        browser.safari = true;
    }
    jQuery.browser = browser;
    jQuery.sub = function() {
        function jQuerySub( selector, context ) {
            return new jQuerySub.fn.init( selector, context );
        }
        jQuery.extend( true, jQuerySub, this );
        jQuerySub.superclass = this;
        jQuerySub.fn = jQuerySub.prototype = this();
        jQuerySub.fn.constructor = jQuerySub;
        jQuerySub.sub = this.sub;
        jQuerySub.fn.init = function init( selector, context ) {
            if ( context && context instanceof jQuery && !(context instanceof jQuerySub) ) {
                context = jQuerySub( context );
            }
            return jQuery.fn.init.call( this, selector, context, rootjQuerySub );
        };
        jQuerySub.fn.init.prototype = jQuerySub.fn;
        var rootjQuerySub = jQuerySub(document);
        return jQuerySub;
    };
})();
var bw;
$(document).ready(function() {
    bw = document.body.clientWidth;
});

// Portfolio Filter
$("#filters").find("li a").on("click", function (event) {
    "use strict";
    event.preventDefault();    
    var filter = $(this).data('filter');
    var item = $(".da-thumbs").find("li");
    $("body").find("#filters li a").removeClass('active');
    $(this).addClass('active');
    item.each(function () {
    var itemfilter = $(this).data('filter');
    if(filter === 'all') {
        item.css('opacity', 1);
        item.removeClass('no-effect');
        item.css('cursor', 'pointer');
        if ($.browser.msie && parseInt(($.browser.version), 10) <= 10){
            item.css('visibility', 'visible');     
        }
            if (bw <= 640) {
            $(this).css('display', 'block');   
        }
    }
    else if(itemfilter === filter) {
        $(this).css('opacity', 1);
        $(this).removeClass('no-effect');
        $(this).css('cursor', 'pointer');
        if ($.browser.msie && parseInt(($.browser.version), 10) <= 10){
            $(this).css('visibility', 'visible');     
        }
        if (bw <= 640) {
            $(this).css('display', 'block');   
        }
    }
    else {
        $(this).css('opacity', 0.2);
        $(this).addClass('no-effect');
        $(this).css('cursor', 'default');
        if ($.browser.msie && parseInt(($.browser.version), 10) <= 10){
            $(this).css('opacity', 0);
            $(this).css('visibility', 'hidden');     
        }
        if (bw <= 640) {
            $(this).css('display', 'none'); 
        }
    }
    });
});   

/* STICKY FILTER MENU */
$(document).ready(function() {
    "use strict";
    fixfilter();
});

var filter = $('body').find('#f-filter');

function fixfilter() {
    "use strict";
    $(window).bind("scroll", function(){
        if ($(this).scrollTop() > 120 && bw >= 900) {
            filter.addClass("f-filter");
        } else {
            filter.removeClass("f-filter");
        }
    });
} 