/*
zeusslider.js 
version: 1.0
description: Zeus - Jquery Responsive Slider Plugin
repository: http://codecanyon.com/user/egemenerd/portfolio
license: http://themeforest.net/licenses
author: egemenerd
*/

$(function($){
	// Registering new tracking handler
	$.fn.iframeTracker = function(handler){
		// Storing the new handler into handler list
		$.iframeTracker.handlersList.push(handler);
		
		// Binding boundary listener
		$(this)
			.bind('mouseover', {handler: handler}, function(e){
				e.data.handler.over = true;
				try{ e.data.handler.overCallback(this); } catch(ex){}
			})
			.bind('mouseout',  {handler: handler}, function(e){
				e.data.handler.over = false;
				$.iframeTracker.focusRetriever.focus();
				try{ e.data.handler.outCallback(this); } catch(ex){}
			});
	};
	
	// Iframe tracker common object
	$.iframeTracker = {
		// Attributes
		focusRetriever: null,  // Element used for restoring focus on window (element)
		focusRetrieved: false, // Says if the focus was retrived on the current page (bool)
		handlersList: [],      // Store a list of every trakers (created by calling $(selector).iframeTracker...)
		isIE8AndOlder: false,  // true for Internet Explorer 8 and older
		
		// Init (called once on document ready)
		init: function(){
			// Determine browser version (IE8-) ($.browser.msie is deprecated since jQuery 1.9)
			try{
				if( $.browser.msie == true && $.browser.version < 9 ){
					this.isIE8AndOlder = true;
				}
			} catch(ex){
				try{
					var matches = navigator.userAgent.match(/(msie) ([\w.]+)/i);
					if( matches[2] < 9 ){
						this.isIE8AndOlder = true;
					}
				} catch(ex2){}
			}
			
			// Listening window blur
			$(window).focus();
			$(window).blur(function(e){
				$.iframeTracker.windowLoseFocus(e);
			});
			
			// Focus retriever
			$('body').append('<div style="position:fixed; top:0; left:0; overflow:hidden;"><input style="position:absolute; left:-300px;" type="text" value="" id="focus_retriever" /></div>');
			this.focusRetriever = $('#focus_retriever');
			this.focusRetrieved = false;
			$(document).mousemove(function(e){ // Focus back to page
				if( document.activeElement && document.activeElement.tagName == 'IFRAME' ){
					$.iframeTracker.focusRetriever.focus();
					$.iframeTracker.focusRetrieved = true;
				}
			});
			// Blur doesn't works correctly on IE8-, so we need to trigger it manually
			if( this.isIE8AndOlder ){
				this.focusRetriever.blur(function(e){
					e.stopPropagation();
					e.preventDefault();
					$.iframeTracker.windowLoseFocus(e);
				});
			}
			
			// Keep focus on window (fix bug IE8- elements focusables)
			if( this.isIE8AndOlder ){
				$('body').click(function(e){ $(window).focus(); });
				$('form').click(function(e){ e.stopPropagation(); });
			}
		},
		
		// Blur on window => calling blurCallback for every handler with over=true
		windowLoseFocus: function(event){
			for(var i in this.handlersList){
				if( this.handlersList[i].over == true ){
					try{ this.handlersList[i].blurCallback(); } catch(ex){}
				}
			}
		}
	};
	
	// Init the iframeTracker on document ready
	$(document).ready(function(){
		$.iframeTracker.init();
	});
});

(function ($) {
    "use strict"; 
    $.fn.zeusslider = function (options) {       
        var selector = $(this);
        // Default settings
        var settings = $.extend({
            sliderheight: 2.5, // Slider height value - 2 - 2.5 - 3 - 3.5 etc.
            customnav: false, // If you want to use custom navigation links, set it true
            nextlink: selector.find('.next-block'), // next
            prevlink: selector.find('.prev-block'), // previous
            autoplay: false, // true or false
            duration: 3000, // Autoplay duration
            infiniteloop: true, // If true, clicking "Next" while on the last slide will transition to the first slide
            customanimation: true, // If true, you can add a custom animated class
            customanimatedclass: '', // You can add a custom animated class
            titlespeed: 800, // Slide title animation speed
            // Callbacks
            onnextslide : function() {},
            onpreviousslide : function() {},
            onslidechange : function() {}
        }, options);

        $(document).ready(function () {
            var slidenumber = selector.find(".zeus-block").length;
            var sliderwidth = selector.find(".zeus-slider").width();
            selector.find(".zeus-slide").css('height', sliderwidth / settings.sliderheight);
            var slideheight = selector.find(".zeus-slide").height();
            if (settings.customnav === false) {
            settings.nextlink.css('top', (slideheight / 2) - 30);
            settings.prevlink.css('top', (slideheight / 2) - 30);
            }
            selector.find(".zeus-slider .zeus-block:first").addClass("s-show");
            slideeffect();
            scaleimage();
            setTimeout(function () {
            selector.find(".zeus-slider").css('background-image', 'none');
            if (slidenumber > 1) {
                if (settings.infiniteloop === false) {
                settings.prevlink.hide();
                settings.nextlink.show();    
                }
                else {
                settings.prevlink.show();
                settings.nextlink.show();
                }
            }
            },500);
        });
        
        // Window resize events
        $(window).resize(function () {
            var sliderwidth = selector.find(".zeus-slider").width();
            selector.find(".zeus-slide").css('height', sliderwidth / settings.sliderheight);
            var slideheight = selector.find(".zeus-slide").height();
            if (settings.customnav === false) {
            settings.nextlink.css('top', (slideheight / 2) - 30);
            settings.prevlink.css('top', (slideheight / 2) - 30);
            }
            scaleimage();
        });
        
        // responsive image
        function scaleimage() {
            var slide = selector.find(".s-show .zeus-slide");
            slide.each(function () {
            var img = $(this).find("img");
            var slideheight = $(this).height();
            var slidewidth = $(this).width();
            var imgheight = img.height();
            var imgwidth = img.width();
            if (slideheight < imgheight) {
                img.css('height', 'auto');
                img.css('width', '100%');
                img.css('margin-left', 0);
                img.css('margin-top', -((imgheight - slideheight) / 2));
            }
            else {
                img.css('height', '100%');
                img.css('width', 'auto');
                img.css('margin-left', -((imgwidth - slidewidth) / 2));
                img.css('margin-top', 0);
            }
            });
            }

        //slide effects
        function slideeffect() {
            var slide = selector.find(".s-show .zeus-slide");
            slide.each(function () {
                var img = $(this).find('img');
                var iframe = $(this).find('iframe');
                var content = $(this).find('.zeus-content');
                var effect = img.data('effect');
                var contenteffect = content.data('effect');
                content.fadeTo(0, 0);
                content.fadeTo(350, 1);
                img.fadeTo(0, 0);
                img.fadeTo(350, 1);
                iframe.fadeTo(0, 0);
                iframe.fadeTo(350, 1);
                img.addClass(effect);
                content.addClass(contenteffect);
                setTimeout(function () {
                    img.removeClass(effect);
                    content.removeClass(contenteffect);
                }, 1000);
            });            
            var info = selector.find(".s-show .zeus-info");            
            info.each(function () {
                $(this).animate({
                'marginBottom': '0px'
            }, settings.titlespeed);
            });            
        }

        // autoplay
        if (settings.autoplay === true) {
            $(document).ready(function () {
                var playslide = setInterval(function () {
                    selector.find(".zeus-info").animate({
                'marginBottom': '-60px'
            }, 100);
                    var $next = selector.find(".s-show").next('.zeus-block');
                    if ($next.length === 0) {
                        selector.find(".s-show").removeClass('s-show');
                        $(".zeus-slider .zeus-block:first").addClass("s-show");
                    } else {
                        selector.find(".s-show").removeClass('s-show').next().addClass('s-show');
                    }
                    setTimeout(function() {
            selector.find(".s-show .zeus-info").animate({
                'marginBottom': '0px'
            }, settings.titlespeed);
            }, 500);
                    if (settings.infiniteloop === false) {
                        if (selector.find('.zeus-block').first().hasClass('s-show')) {
                            settings.prevlink.fadeOut();
                        } else {
                            settings.prevlink.delay(500).fadeIn();
                        }
                        if (selector.find('.zeus-block').last().hasClass('s-show')) {
                            settings.nextlink.fadeOut();
                        } else {
                            settings.nextlink.delay(500).fadeIn();
                        }
                    }
                    slideeffect();
                    scaleimage();
                    settings.onslidechange.call(this);
                }, settings.duration);
                
                // pause on navigation clicks
                settings.nextlink.on("click", function () {
                    clearInterval(playslide);
                    playslide = 0;
                });
                
                settings.prevlink.on("click", function () {
                    clearInterval(playslide);
                    playslide = 0;
                });
                selector.find('iframe').iframeTracker({
                    blurCallback: function(){
                        clearInterval(playslide);
                        playslide = 0;
                    }
                });
            });
        }

        //navigation arrows
        settings.nextlink.on("click", function () {
                selector.find(".zeus-info").animate({
                'marginBottom': '-60px'
            }, 100);
            var $next = selector.find(".s-show").next('.zeus-block');
            if ($next.length === 0) {
                selector.find(".s-show").removeClass('s-show');
                selector.find(".zeus-slider .zeus-block:first").addClass("s-show");
            } else {
                selector.find(".s-show").removeClass('s-show').next().addClass('s-show');
            }
            setTimeout(function() {
            selector.find(".s-show .zeus-info").animate({
                'marginBottom': '0px'
            }, settings.titlespeed);
            }, 500);
            
            if (settings.infiniteloop === false) {
                if (selector.find('.zeus-block').first().hasClass('s-show')) {
                    settings.prevlink.fadeOut();
                } else {
                    settings.prevlink.delay(500).fadeIn();
                }
                if (selector.find('.zeus-block').last().hasClass('s-show')) {
                    settings.nextlink.fadeOut();
                } else {
                    settings.nextlink.delay(500).fadeIn();
                }
            }
            slideeffect();            
            scaleimage();   
            $(document).ready(function(){    
                scaleimage();  
            });
            settings.onslidechange.call(this);
            settings.onnextslide.call(this);

        });

        settings.prevlink.on("click", function () {
            selector.find(".zeus-info").animate({
                'marginBottom': '-60px'
            }, 100);
            var $prev = selector.find(".s-show").prev('.zeus-block');
            if ($prev.length === 0) {
                selector.find(".s-show").removeClass('s-show');
                selector.find(".zeus-slider .zeus-block:last").addClass("s-show");
            } else {
                selector.find(".s-show").removeClass('s-show').prev().addClass('s-show');
            }
            setTimeout(function() {
            selector.find(".s-show .zeus-info").animate({
                'marginBottom': '0px'
            }, settings.titlespeed);
            }, 500);
            if (settings.infiniteloop === false) {
                if (selector.find('.zeus-block').first().hasClass('s-show')) {
                    settings.prevlink.fadeOut();
                } else {
                    settings.prevlink.delay(500).fadeIn();
                }
                if (selector.find('.zeus-block').last().hasClass('s-show')) {
                    settings.nextlink.fadeOut();
                } else {
                    settings.nextlink.delay(500).fadeIn();
                }
            }
            slideeffect();           
            scaleimage();            
            settings.onslidechange.call(this);
            settings.onpreviousslide.call(this);

        });
    };

})(jQuery);