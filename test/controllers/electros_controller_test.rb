require 'test_helper'

class ElectrosControllerTest < ActionController::TestCase
  setup do
    @electro = electros(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:electros)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create electro" do
    assert_difference('Electro.count') do
      post :create, electro: { about: @electro.about, img: @electro.img, name: @electro.name }
    end

    assert_redirected_to electro_path(assigns(:electro))
  end

  test "should show electro" do
    get :show, id: @electro
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @electro
    assert_response :success
  end

  test "should update electro" do
    patch :update, id: @electro, electro: { about: @electro.about, img: @electro.img, name: @electro.name }
    assert_redirected_to electro_path(assigns(:electro))
  end

  test "should destroy electro" do
    assert_difference('Electro.count', -1) do
      delete :destroy, id: @electro
    end

    assert_redirected_to electros_path
  end
end
