class CreateProjects < ActiveRecord::Migration
  def change
    create_table :projects do |t|
      t.string :img
      t.string :date
      t.text :about

      t.timestamps
    end
  end
end
