class RemoveStylesFromLamps < ActiveRecord::Migration
  def change
    remove_column :lamps, :styles, :string
  end
end
