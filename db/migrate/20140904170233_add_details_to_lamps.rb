class AddDetailsToLamps < ActiveRecord::Migration
  def change
    add_column :lamps, :action, :boolean
    add_column :lamps, :cost, :integer
  end
end
