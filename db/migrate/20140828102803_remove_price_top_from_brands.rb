class RemovePriceTopFromBrands < ActiveRecord::Migration
  def change
    remove_column :brands, :price_top, :integer
  end
end
