class RemoveNewFromLamps < ActiveRecord::Migration
  def change
    remove_column :lamps, :new, :boolean
  end
end
