class RemovePriceBottomFromBrands < ActiveRecord::Migration
  def change
    remove_column :brands, :price_bottom, :integer
  end
end
