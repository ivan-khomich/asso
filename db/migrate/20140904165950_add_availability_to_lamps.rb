class AddAvailabilityToLamps < ActiveRecord::Migration
  def change
    add_column :lamps, :availability, :integer
  end
end
