class AddNewToLamps < ActiveRecord::Migration
  def change
    add_column :lamps, :new, :boolean
  end
end
