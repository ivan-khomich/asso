class RemoveLinkFromLamps < ActiveRecord::Migration
  def change
    remove_column :lamps, :link, :string
  end
end
