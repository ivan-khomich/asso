class AddDescriptionToLamps < ActiveRecord::Migration
  def change
    add_column :lamps, :description, :string
  end
end
