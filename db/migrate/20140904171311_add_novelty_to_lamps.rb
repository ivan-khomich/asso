class AddNoveltyToLamps < ActiveRecord::Migration
  def change
    add_column :lamps, :novelty, :boolean
  end
end
