class AddTypeOfProductionToLamps < ActiveRecord::Migration
  def change
    add_column :lamps, :type_of_production, :string
  end
end
