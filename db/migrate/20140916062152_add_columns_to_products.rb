class AddColumnsToProducts < ActiveRecord::Migration
  def change
    add_column :products, :availability, :bool
    add_column :products, :new_product, :bool
  end
end
