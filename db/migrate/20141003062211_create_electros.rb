class CreateElectros < ActiveRecord::Migration
  def change
    create_table :electros do |t|
      t.string :img
      t.string :name
      t.text :about

      t.timestamps
    end
  end
end
