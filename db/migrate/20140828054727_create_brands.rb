class CreateBrands < ActiveRecord::Migration
  def change
    create_table :brands do |t|
      t.string :name
      t.string :picture
      t.integer :price_top
      t.integer :price_bottom
      t.string :link
      t.string :styles

      t.timestamps
    end
  end
end
