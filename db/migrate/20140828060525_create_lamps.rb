class CreateLamps < ActiveRecord::Migration
  def change
    create_table :lamps do |t|
      t.string :name
      t.string :picture
      t.string :type
      t.references :brand, index: true
      t.string :link
      t.string :prem_class
      t.string :styles

      t.timestamps
    end
  end
end
