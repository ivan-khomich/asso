class RemoveTypeFromLamps < ActiveRecord::Migration
  def change
    remove_column :lamps, :type, :string
  end
end
