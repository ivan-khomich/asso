class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :img
      t.integer :price
      t.text :about
      t.integer :size
      t.integer :amount
      t.integer :article
      t.string :brand
      t.string :metal_color
      t.string :glass_color

      t.timestamps
    end
  end
end
