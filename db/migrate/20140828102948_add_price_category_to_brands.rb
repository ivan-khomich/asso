class AddPriceCategoryToBrands < ActiveRecord::Migration
  def change
    add_column :brands, :price_category, :string
  end
end
