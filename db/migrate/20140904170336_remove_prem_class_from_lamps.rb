class RemovePremClassFromLamps < ActiveRecord::Migration
  def change
    remove_column :lamps, :prem_class, :string
  end
end
