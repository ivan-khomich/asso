# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141003062211) do

  create_table "brands", force: true do |t|
    t.string   "name"
    t.string   "picture"
    t.string   "link"
    t.string   "styles"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "price_category"
    t.string   "img2"
  end

  create_table "electros", force: true do |t|
    t.string   "img"
    t.string   "name"
    t.text     "about"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "lamps", force: true do |t|
    t.string   "name"
    t.string   "picture"
    t.integer  "brand_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "availability"
    t.boolean  "action"
    t.integer  "cost"
    t.string   "description"
    t.boolean  "novelty"
    t.string   "type_of_production"
  end

  add_index "lamps", ["brand_id"], name: "index_lamps_on_brand_id"

  create_table "news", force: true do |t|
    t.string   "title"
    t.text     "body"
    t.string   "img"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

# Could not dump table "products" because of following NoMethodError
#   undefined method `[]' for nil:NilClass

  create_table "projects", force: true do |t|
    t.string   "img"
    t.string   "date"
    t.text     "about"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name"
    t.string   "img2"
  end

end
