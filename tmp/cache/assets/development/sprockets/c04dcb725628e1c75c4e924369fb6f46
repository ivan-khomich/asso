{I"
class:ETI"ProcessedAsset; FI"logical_path; TI"style.css.orig; FI"pathname; TI"0$root/app/assets/stylesheets/style.css.orig; FI"content_type; TI"text/css; TI"
mtime; Tl+FoSI"length; TirI"digest; TI"%a05e9a017f3c985d2d5ded93ad42085a; FI"source; TI"r/* ---------------------- 
Stylesheet Guide
-------------------------

FONTS
GENERAL STYLES
MAIN MENU
PAGE STRUCTURE
ICONS
PORTFOLIO
BLOG
SIDEBAR
COMMENTS
FORMS
RESPONSIVE IFRAME
FLEX IMAGE
NUMERIC LIST
ACCORDION
TESTIMONIALS
FOOTER
FLICKR
SOCIAL ICONS
BACK TO TOP BUTTON
TABS
UNDER CONSTRUCTION


*/

/* ================= FONTS ================== */

@font-face {
    font-family:'open_sansregular';
    src: url('../fonts/opensans-regular-webfont.eot');
    src: url('../fonts/opensans-regular-webfont.eot?#iefix') format('embedded-opentype'), url('../fonts/opensans-regular-webfont.woff') format('woff'), url('../fonts/opensans-regular-webfont.ttf') format('truetype'), url('../fonts/opensans-regular-webfont.svg#open_sansregular') format('svg');
    font-weight: normal;
    font-style: normal;
}
@font-face {
    font-family:'oswaldregular';
    src: url('../fonts/oswald-regular-webfont.eot');
    src: url('../fonts/oswald-regular-webfont.eot?#iefix') format('embedded-opentype'), url('../fonts/oswald-regular-webfont.woff') format('woff'), url('../fonts/oswald-regular-webfont.ttf') format('truetype'), url('../fonts/oswald-regular-webfont.svg#oswaldregular') format('svg');
    font-weight: normal;
    font-style: normal;
}
body, p {
    font-family:'open_sansregular';
}
h1, h2, h3, h4, h5, h6, .nav, .fit-text, #modal ul li:before, .toggleMenu,.countdown,.zeus-info,.da-thumbs li a div span,.faq dt,.st-accordion ul li > a,.zeus-text-right,.zeus-text-left, #filters li a, .zeus-text-center {
    font-family: 'Oswald', sans-serif;
    font-weight:normal;
    text-transform:uppercase;
}
/* ================= GENERAL STYLES ================== */

html, body{
	margin: 0;
	padding: 0;
}
body{
    font-size:13px;
    -webkit-overflow-scrolling: touch;
}
img {
    -ms-interpolation-mode:bicubic;
    image-rendering:optimizeQuality;
}
h1, h2, h3, h4, h5, h6 {
    margin-top:0px;
    margin-bottom:15px;
    padding:0px;
    line-height:1;
}
h1 {
    font-size:48px;
}
h2 {
    font-size:36px;
}
h3 {
    font-size:28px;
}
h4 {
    font-size:24px;
}
h5 {
    font-size:20px;
}
h6 {
    font-size:18px;
}
.show
{
    display:block !important;
}
.hide
{
    display:none !important;
}
.clear {
    clear:both;
}

p {
    font-size:13px;
    line-height:1.5;
    margin-bottom:21px;
    margin-top:0px !important;
    font-weight:normal;
    word-spacing: 0.2em;
}
p:last-child
{
    margin-bottom:0px;
}
a {
	-webkit-transition: color 0.2s ease-in-out;
	-moz-transition: color 0.2s ease-in-out;
	-o-transition: color 0.2s ease-in-out;
	-ms-transition: color 0.2s ease-in-out;
	text-decoration:none;
}
a:hover {
	text-decoration:underline;
}
hr {
    height:1px;
    width:auto;
    margin-left:-30px;
    margin-right:-30px;
    margin-top:30px;
    margin-bottom:30px;
    clear:both;
    outline:none;
    border:none;
}
label
{
    font-weight:normal;
    font-size:13px;
    line-height:1.7;
}
.label {
    padding:5px 7px 5px 7px;
    margin-bottom:5px;
}
blockquote {
    line-height: 1.5;
    padding-left: 15px;
    padding-right: 10px;
    padding-top: 10px;
    padding-bottom: 10px;
    margin: 0px 0px 21px 0px;
    position: relative;
}
blockquote cite {
    font-size:14px;
    line-height:1.5;
    display:block;
    margin-top:20px;
    text-align:right;
}
.subtitle {
    margin:0px;
    padding:15px 20px 15px 30px;
    position:relative;
}

/* ================= MAIN MENU  ================== */
.logo {
    float:left;
    padding-left:30px;
    font-size:60px;
    height:97px;
}
.logo img {
    height:97px;
    width:auto;
    padding-top:10px;
    padding-bottom:10px;
    -webkit-transition: all 0.2s ease-in-out;
	-moz-transition: all 0.2s ease-in-out;
	-o-transition: all 0.2s ease-in-out;
	-ms-transition: all 0.2s ease-in-out;
}
 .nav-container {
    width: 100%;
    max-width:1024px;
    margin:0px auto;
    position:relative;
    height:103px;
    -webkit-transition: all 0.2s ease-in-out;
	-moz-transition: all 0.2s ease-in-out;
	-o-transition: all 0.2s ease-in-out;
	-ms-transition: all 0.2s ease-in-out;
}
.toggleMenu {
    display: none;
    list-style:none;
    padding:15px 35px;
    margin:0px;
    left:0;
    top:0;
    width:100%;
    z-index:9;
    font-size:20px;
}
.toggleMenu:hover {
    text-decoration:none;
}
nav {
    float:right;
}
.nav {
    font-size:14px;
    list-style: none;
    *zoom: 1;
    padding:0px;
    padding-right:30px;
    margin: 0;
    line-height:90px;
    z-index:9999;
    pointer-events:none;
    float:right;
    -webkit-transition: all 0.2s ease-in-out;
	-moz-transition: all 0.2s ease-in-out;
	-o-transition: all 0.2s ease-in-out;
	-ms-transition: all 0.2s ease-in-out;
}
.nav ul {
    list-style: none;
    width: 14em;
    padding:0px;
    margin:0px;
    z-index:99999;
    line-height:40px;
}
.nav a {
    padding: 5px 15px;
    text-decoration:none;
    -webkit-transition: background 0.2s ease-in-out;
    -moz-transition: background 0.2s ease-in-out;
    -o-transition: background 0.2s ease-in-out;
    -ms-transition: background 0.2s ease-in-out;
    transition: background 0.2s ease-in-out;
}
.nav ul li a {
    padding: 0px 20px 0px 20px;
}
.nav ul li:first-child {
    margin-top: 10px;
}
.nav ul li:last-child {
    margin-bottom: 10px;
}
.nav li {
    position: relative;
}
.nav > li {
    float: left;
}
.nav > li > a {
    display: block;
}
.nav li ul {
    position: absolute;
    left: -9999px;
}
.nav > li.hover > ul {
    left: 0;
}
.nav li li.hover ul {
    left: 100%;
    top: 0;
    padding-top:0px;
}
.nav li li a {
    display: block;
    position: relative;
    z-index:100;
    border-bottom:none;
}
.nav li li li a {
    z-index:200;
}
.nav li li a:hover, .nav li li li a :hover {
    border-bottom:none;
}
.fix-nav {
    z-index: 99999 !important;
    position: fixed;
    top: 0 !important;
    width: 100%;
}
.f-nav > nav >.nav {
    line-height:60px;
}
.f-nav > .logo {
    height:67px;
}
.f-nav > .logo img {
    height:67px;
}
.f-nav {
    height:73px;
    opacity:0.9;
}
/* ================= PAGE STRUCTURE ================== */
#left-bg,#right-bg,#right-bg-light,#left-bg-dark {
    position:fixed;
    width:50%;
    height:100%;
    top:0;
    z-index:-1;
    opacity:0.7;
}
#left-bg,#left-bg-dark {
    left:0;
}
#right-bg,#right-bg-light {
    right:0;
}
.maincontainer {
    width:100%;
    max-width:1024px;
    position:relative;
    margin:0px auto 0px auto;
    overflow: hidden;
    padding-top:100px;
}
.darkcontainer, .lightcontainer {
    width:100%;
    padding-bottom:15px;
    padding-top:15px;
    position:relative;
}
.leftcontainer, .rightcontainer {
    width:50%;
    padding:30px;
    margin-bottom: -2000px; /* equal height css trick */
    padding-bottom: 2030px; /* equal height css trick */
}
.leftcontainer {
    float:left;
}
.rightcontainer {
    float:right;
}
.pageimage {
    width:100%;
    position:relative;
}
.pageimage img{
    width:100%;
    height:auto;
    margin:0px;
    vertical-align:bottom;
}

/* ================= ICONS  ================== */
 .icon {
    width: 50%;
    float: left;
    text-align: center;
    display: inline-block;
    margin: 0px;
    position:relative;
}
.icon img {
    margin: 20px;
    width:48px;
    height:48px;
}
.icon .circle {
    padding: 10px;
    width: 110px;
    height: 110px;
    display: inline-block;
    margin-bottom:5px;
    font-size:70px;
    line-height:90px;
    text-align:center;
    position:relative;
    -webkit-border-radius: 100%;
    -moz-border-radius: 100%;
    border-radius: 100%;
    cursor:pointer;
    -webkit-animation-name: spin;
	-webkit-animation-iteration-count: 1;
	-webkit-animation-timing-function: linear;
	-webkit-animation-duration: 0.4s;
	-moz-animation-name: spin;
	-moz-animation-iteration-count: 1;
	-moz-animation-timing-function: linear;
	-moz-animation-duration: 0.4s;
	-o-animation-name: spin;
	-o-animation-iteration-count: 1;
	-o-animation-timing-function: linear;
	-o-animation-duration: 0.4s;
	-ms-animation-name: spin;
	-ms-animation-iteration-count: 1;
	-ms-animation-timing-function: linear;
	-ms-animation-duration: 0.4s;
}
.icon .circle:hover {
    -webkit-animation-name: spin2;
	-webkit-animation-iteration-count: 1;
	-webkit-animation-timing-function: linear;
	-webkit-animation-duration: 0.4s;
	-moz-animation-name: spin2;
	-moz-animation-iteration-count: 1;
	-moz-animation-timing-function: linear;
	-moz-animation-duration: 0.4s;
	-o-animation-name: spin2;
	-o-animation-iteration-count: 1;
	-o-animation-timing-function: linear;
	-o-animation-duration: 0.4s;
	-ms-animation-name: spin2;
	-ms-animation-iteration-count: 1;
	-ms-animation-timing-function: linear;
	-ms-animation-duration: 0.4s;
}
.icon h5 {
    margin-top:15px !important;
    margin-bottom:10px !important;
    border:none !important;
    padding:0px !important;
}
.icon p {
    text-align:center;
    margin-top:0px;
    margin-bottom:30px;
    padding-right:10px;
    padding-left:10px;
}
/* ================= PORTFOLIO ================== */
 #filters {
    list-style:none;
    padding:25px 15px 25px 30px;
    margin:0;
    display:inline-block;
    position:relative;
    width:100%;
    vertical-align:bottom;
}
#filters li {
    float:left;
    margin-right:20px;
}
#filters li a {
    padding:5px 10px 5px 10px;
    font-size:16px;
    -webkit-transition: background-color 0.2s ease-in-out;
    -moz-transition: background-color 0.2s ease-in-out;
    -o-transition: background-color 0.2s ease-in-out;
    -ms-transition: background-color 0.2s ease-in-out;
}
#filters li a:hover {
    text-decoration:none;
}
.f-filter {
    z-index: 9999 !important;
    position:fixed;
    top: 73px;
    left:0;
    right:0;
    margin:0px auto;
    max-width:1024px;
    width: 100%;
    -webkit-transition: all 0.2s linear;
	-moz-transition: all 0.2s linear;
	-o-transition: all 0.2s linear;
	-ms-transition: all 0.2s linear;
    opacity:0.9;
}
.f-filter  #filters {
    padding:15px 15px 15px 30px; 
}
.f-filter #filters li {
    margin-right:10px;
    -webkit-transition: all 0.2s linear;
	-moz-transition: all 0.2s linear;
	-o-transition: all 0.2s linear;
	-ms-transition: all 0.2s linear;
}
.da-thumbs {
    list-style:none;
    position:relative;
    margin:0px;
    padding:0px;
    display:inline-block;
    vertical-align: bottom;
}
.da-thumbs li {
    float:left;
    position:relative;
    width:25%;
    -webkit-transition: all 500ms cubic-bezier(0.645, 0.045, 0.355, 1.000); 
    -moz-transition: all 500ms cubic-bezier(0.645, 0.045, 0.355, 1.000); 
    -ms-transition: all 500ms cubic-bezier(0.645, 0.045, 0.355, 1.000); 
    -o-transition: all 500ms cubic-bezier(0.645, 0.045, 0.355, 1.000); 
    transition: all 500ms cubic-bezier(0.645, 0.045, 0.355, 1.000);
    -webkit-transition-timing-function: cubic-bezier(0.645, 0.045, 0.355, 1.000); 
    -moz-transition-timing-function: cubic-bezier(0.645, 0.045, 0.355, 1.000); 
    -ms-transition-timing-function: cubic-bezier(0.645, 0.045, 0.355, 1.000); 
    -o-transition-timing-function: cubic-bezier(0.645, 0.045, 0.355, 1.000); 
    transition-timing-function: cubic-bezier(0.645, 0.045, 0.355, 1.000);
}
.da-thumbs.halfpage li {
    width:50%;
}
.da-thumbs.halfpage {
    background-color:transparent !important;
    margin-bottom:20px;
}
.da-thumbs li a, .da-thumbs li a img {
    display:block;
    position:relative;
    width:100%;
}
.da-thumbs li a {
    overflow:hidden;

}
.da-thumbs li a div {
    position:absolute;
    width:100%;
    height:100%;
    opacity:0.9;
}
.da-thumbs li a div span {
    display:block;
    padding:10px 0;
    margin:20% 20px 0px 20px;
    text-align:center;
    font-size:16px;
}
.no-effect{
    pointer-events:none !important;
  -webkit-transform: rotate(360deg);
     -moz-transform: rotate(360deg);
       -o-transform: rotate(360deg);
      -ms-transform: rotate(360deg);
          transform: rotate(360deg);
}
/* ================= BLOG ================== */
.blogcontainer {
    width:100%;
    position:relative;
    display:inline-block;
    vertical-align: bottom;
}
.noborder {
    border:none;
}
.blogimage {
    width:50%;
    float:left;
    overflow: hidden;
    position: relative;
}
.blogimage img {
    width:100%;
    height:auto;
    vertical-align: bottom;
    display: block;
    position: relative;
    opacity:0.7;
	-moz-transition: all 0.3s ease-in;
	-webkit-transition: all 0.3s ease-in;
	-o-transition: all 0.3s ease-in;
	-ms-transition: all 0.3s ease-in;
	transition: all 0.3s ease-in;
}
.blogexcerpt {
    width:50%;
    float:right;
    padding:30px;
}
.blogimage .mask {
   position: absolute;
   overflow: hidden;
   top: 0;
   left: 0;
   cursor:pointer;
   opacity:1;
   visibility:visible;
   border-style:solid;
   -moz-box-sizing:border-box;
   -webkit-box-sizing:border-box;
   box-sizing:border-box;
   -moz-transition: all 0.4s cubic-bezier(0.940, 0.850, 0.100, 0.620);
   -webkit-transition: all 0.4s cubic-bezier(0.940, 0.850, 0.100, 0.620);
   -o-transition: all 0.4s cubic-bezier(0.940, 0.850, 0.100, 0.620);
   -ms-transition: all 0.4s cubic-bezier(0.940, 0.850, 0.100, 0.620);
   transition: all 0.4s cubic-bezier(0.940, 0.850, 0.100, 0.620);
}
.blogimage:hover .mask {
	opacity:0;
	visibility:hidden;
}
.blogimage:hover img {
	opacity:1;
}
.blogdate {
    margin-bottom:15px;
    text-transform:uppercase;
}
.blogpager {
    width:100%;
    position:relative;
    padding:10px 30px 10px 30px;
    display:inline-block;
    vertical-align:bottom;
}
.blogpager .button {
    margin: 0px !important;
}
.next, .previous {
    font-size:14px;
}
.previous {
    float:left;
    width:50%;
}
.next {
    float:right;
    width:50%;
    text-align:right;
}
.featuredimage, .featuredimage2 {
    position:absolute;
    margin-left:-30px;
    margin-top:-30px;
}

/* ==== SIDEBAR ==== */

.sidebarcontainer {
    padding-right: 30px;
    padding-left: 30px;
    padding-top: 15px;
    padding-bottom: 15px;
    position: relative;
    width: 100%;
    display: inline-block;
}
.sidebarbox {
    clear: both;
    margin-bottom: 30px;
}
.leftsidebarcontainer {
    width: 65%;
    padding-right: 15px;
    float: left;
}
.leftsidebarcontainer hr {
    margin-right:0px;
}
.rightsidebarcontainer {
    width: 35%;
    float: right;
    padding-left: 15px;
}
.rightsidebarcontainer .sidebarlist {
    list-style:none;
    padding:0;
    margin:0;
}
.rightsidebarcontainer .sidebarlist li {
    padding-bottom:10px;
    margin-bottom:10px;
}
.rightsidebarcontainer .sidebarlist li:last-child {
    padding-bottom:0px;
    margin-bottom:0px;
    border-bottom:none;
}
.rightsidebarcontainer .sidebarlist li a:hover {
    text-decoration:none;
}
.tags {
    float: left;
    margin-right: 5px;
    margin-bottom: 5px;
    padding: 5px 7px 5px 7px;
    -webkit-transition: background-color 0.2s ease-in-out;
    -moz-transition: background-color 0.2s ease-in-out;
    -o-transition: background-color 0.2s ease-in-out;
    -ms-transition: background-color 0.2s ease-in-out;
}
.tags:hover {
    text-decoration: none;
}
.rightsidebarcontainer .cbp-qtrotator img {
    max-width: 90px;
    height:auto;
}

/* ================= COMMENTS ================== */
.comments {
    margin-top:20px;
    margin-bottom:0px;
}
.comments p {
    margin-bottom:0px !important;
    padding-right:35px;
}
.comments .meta {
    margin-bottom:10px !important;
    text-transform:uppercase;
}
.comments_content {
    margin-bottom:30px;
    padding-right:15px;
    padding-top:10px;
    position:relative;
}
.reply {
    padding-left:40px;
}
.reply_icon {
    width:31px;
    height:33px;
    background-image:url(../images/reply.png);
    position:absolute;
    display:none;
    top:-40px;
    left:-45px;
    background-repeat:no-repeat;
}
.comments.reply .reply_icon {
    display:block !important;
}
.comments a.reply {
    color:#fff !important;
    font-size:12px;
    position:absolute;
    top:-1px;
    right:0;
    padding:7px;
    background-color:#c7c7c7;
    -webkit-transition:background-color 0.4s ease-in-out;
    -moz-transition:background-color 0.4s ease-in-out;
    -o-transition:background-color 0.4s ease-in-out;
    transition:background-color 0.4s ease-in-out;
}
.comments a.reply:hover {
    background-color:#da2f10;
    text-decoration:none;
}
/* ================= FORMS ================== */

input[type="text"], input[type="email"], input[type="number"], input[type="date"], input[type="password"], textarea
{
    width: 80%;
    display: block;
    font-size: 14px;
    margin: 0px 0px 11px 0px;
    padding: 5px 10px 5px 10px;
    height: 35px;
    -webkit-transition: all 0.4s linear;
    -moz-transition: all 0.4s linear;
    -o-transition: all 0.4s linear;
    transition: all 0.4s linear;
    background-color:rgba(255,255,255,0.1);
}
input[type="text"].oversize, input[type="email"].oversize, input[type="number"].oversize, input[type="date"].oversize {
	font-size:15px;
	padding:4px 5px
}
input[type="text"]:focus, input[type="email"]:focus, input[type="number"]:focus, input[type="date"]:focus, textarea:focus, input[type="password"]:focus {
	outline:none !important;
}
input[type="submit"] {
	margin-top:14px;
}
textarea {
	height:100px;
	width:100%;
	margin-bottom:0px !important;
}
.button {
	width: auto;
	cursor: pointer;
	display: inline-block;
	font-size: 14px;
	line-height: 1;
	margin: 5px 20px 0px 0px;
	outline: none;
	padding: 10px 20px 11px;
	position: relative;
	text-align: center;
	text-decoration: none !important;
	-webkit-transition: background-color 0.15s ease-in-out;
	-moz-transition: background-color 0.15s ease-in-out;
	-o-transition: background-color 0.15s ease-in-out;
	transition: background-color 0.15s ease-in-out;
	border: none !important;
}
.searchbox .button {
    -webkit-border-radius: 0px !important;
    -moz-border-radius: 0px !important;
    border-radius: 0px !important;
}
.searchbox {
    width:100% !important;
    display:inline-block;
    position:relative;
}
.searchbox input[type="text"].searchtext {
    width:75% !important;
    float:left;
    margin: 0px;
}
.searchbox .button {
    width:25% !important;
    float:right;
    position:absolute;
    top:0;
    margin:0px;
}
/* ================= RESPONSIVE IFRAME ================== */
 .flex-video {
    position:relative;
    padding-bottom:67.5%;
    height:0;
    overflow:hidden;
    margin-bottom:20px;
}
.flex-video.widescreen {
    padding-bottom:37.25%
}
.flex-video.vimeo {
    padding-top:0
}
.flex-video iframe, .flex-video object, .flex-video embed, .flex-video video {
    position:absolute;
    top:0;
    left:0;
    width:100%;
    height:100%;
    outline:none;
    border:none;
}
/* ================= FLEX IMAGE ================== */
.caption-image {
    position:relative;
    margin-bottom:20px;
}
.caption-image img {
    width:100%;
    height:auto;
}
.caption-image figcaption {
    position: absolute;
    bottom: 0;
    left:0;
    padding:10px;
    width:100%;
    text-align:center;
}
/* ================= NUMERIC LIST ================== */
.faq {
    counter-reset: number-counter;
    margin:0px;
    padding:0px;
}
.faq dt {
    position: relative;
    font-size:20px;
}
.faq dt:before {
    content: counter(number-counter);
    counter-increment: number-counter;
    position: absolute;
    left: 0;
    top: 0;
    font: bold 55px/1 Sans-Serif;
}
.faq dd {
    margin: 0 0 25px 0;
}	
.faq dd:last-child {
    margin: 0;
}
.faq dt, .faq dd {
    padding-left: 55px;
}
/* ================= ACCORDION ================== */
.st-accordion {
    width:100%;
    margin:0px;
    position:relative;
}
.st-accordion ul {
    margin:0;
    padding:0;
    margin-bottom:20px;
}
.st-accordion ul li {
    height: 70px;
    overflow: hidden;
}
.st-accordion ul li:first-child {
    border-top:none;
}
.st-accordion ul li > a {
    font-size: 18px;
    display: block;
    position: relative;
    line-height: 70px;
    outline:none;
    -webkit-transition: color 0.2s ease-in-out;
    -moz-transition: color 0.2s ease-in-out;
    -o-transition: color 0.2s ease-in-out;
    -ms-transition: color 0.2s ease-in-out;
    transition: color 0.2s ease-in-out;
}
.st-accordion ul li > a .st-arrow {
    background: transparent url(../images/down-arrow.png) no-repeat center center;
    text-indent:-9000px;
    width: 34px;
    height: 21px;
    position: absolute;
    top: 46%;
    right: -26px;
    margin-top: -7px;
    opacity:0;
    -webkit-transition: all 0.2s ease-in-out;
    -moz-transition: all 0.2s ease-in-out;
    -o-transition: all 0.2s ease-in-out;
    -ms-transition: all 0.2s ease-in-out;
    transition: all 0.2s ease-in-out;
}
.st-accordion ul li > a:hover {
    text-decoration:none;
}
.st-accordion ul li > a:hover .st-arrow {
    opacity:1;
    right: 10px;
}
.st-accordion ul li.st-open > a span {
    -webkit-transform:rotate(180deg);
    -moz-transform:rotate(180deg);
    transform:rotate(180deg);
    right:10px;
    opacity:1;
}
.st-content {
    padding: 0px 0px 30px 0px;
}
.st-content img{
    width:30%;
    float:left;
    height:auto;
    margin-right:20px;
    vertical-align:bottom;
}
/* ================= TESTIMONIALS ================== */
.cbp-qtrotator {
	position: relative;
	margin: 0;
	width: 100%;
}
.cbp-qtrotator .cbp-qtcontent {
	position: absolute;
	padding-top: 29px;
	top: 0;
	z-index: 0;
	opacity: 0;
	width: 100%;
}
.cbp-qtrotator img {
    max-width: 120px;
    height:auto;
}
.no-js .cbp-qtrotator .cbp-qtcontent {
	border-bottom: none;
}
.cbp-qtrotator .cbp-qtcontent.cbp-qtcurrent,
.no-js .cbp-qtrotator .cbp-qtcontent {
	position: relative; 
	z-index: 100;
	pointer-events: auto;
	opacity: 1;
}
.cbp-qtrotator .cbp-qtcontent:before,
.cbp-qtrotator .cbp-qtcontent:after {
	content: " ";
	display: table;
}
.cbp-qtrotator .cbp-qtcontent:after {
	clear: both;
}
.cbp-qtprogress {
	position: absolute;
	height: 1px;
	width: 0%;
	top: 0;
	z-index: 1000;
}
.cbp-qtrotator .blockquote {
	margin: 0;
	padding: 0;
}
.cbp-qtrotator .blockquote .footer {
	font-size: 14px;
}
.cbp-qtrotator .blockquote .footer:before {
	content: '― ';
}
.cbp-qtrotator .cbp-qtcontent img {
	float: right;
	margin-left: 25px;
}

/* ================= FOOTER ================== */
 footer {
    height:100%;
    margin:0px auto 0px auto;
    width:100%;
    padding-bottom:7px;
    position:relative;
    overflow:hidden;
    position:relative;
}
#footer-widgets {
    position:relative;
    max-width:1024px;
    width:100%;
    height:auto;
    clear:both;
    display:inline-block;
    padding:30px;
    padding-bottom:0px;
}
#footer-widgets h5{
    margin-bottom:20px;
}
.footer-widget {
    height:auto;
    float:left;
    padding-bottom:30px;
}
.footer-widget ul {
    list-style:none;
    padding:0;
    margin:0;
}
.footer-widget ul li {
    padding-bottom:10px;
    margin-bottom:10px;
}
.footer-widget ul li:last-child {
    padding-bottom:0px;
    margin-bottom:0px;
    border-bottom:none;
}
.footer-widget ul li a:hover {
    text-decoration:none;
}
.first-clmn {
    padding-right:20px;
    width:30%;
}
.second-clmn {
    padding-right:10px;
    padding-left:10px;
    width:30%;
}
.third-clmn {
    padding-left:20px;
    width:40%;
}
.credits {
    float:left;
    font-size:11px;
    padding-top:16px;
    padding-left:30px;
}
/* ================= FLICKR  ================== */
 .flickr-box {
    margin: 0px;
    margin-right:-5px;
    padding: 0px;
    overflow: hidden;
    width: 110%;
}
.flickr-box li {
    list-style:none;
    float:left;
    margin-right:5px;
    margin-bottom:5px;
    padding:0px !important;
    height:80px !important;
    background-image:none !important;
}
.flickr-box li:last-child {
    margin-right:0px;
}
.flickr-box li {
    border:none !important;
}
.flickr-box li img {
    display:block;
    -webkit-transition:all 0.4s ease-in-out;
    -moz-transition:all 0.4s ease-in-out;
    -o-transition:all 0.4s ease-in-out;
    transition:all 0.4s ease-in-out;
    border:5px solid transparent;
    width: 87px;
    border:3px solid #262626;
    opacity:0.5;
}
.flickr-box li img:hover {
    border:3px solid #fff;
    opacity:1;
}
/* ================= SOCIAL ICONS ================== */
 .social-icons {
    list-style-type:none;
    display:block;
    margin:10px 30px 0px 0px;
    padding:0px;
    float:right;
}
.social-icons li {
    float:left;
    display:block;
    margin-left:10px;
    padding:0;
}
.social-icons li img {
    width:32px;
    height:auto;
}
.social-icon {
    -webkit-transition:all 0.2s ease-in-out;
    -moz-transition:all 0.2s ease-in-out;
    -o-transition:all 0.2s ease-in-out;
    -ms-transition:all 0.2s ease-in-out;
    transition:all 0.2s ease-in-out;
    -webkit-border-radius: 100%;
    -moz-border-radius: 100%;
    border-radius: 100%;
    padding:5px;
}
.social-icon:hover {
    -webkit-border-radius: 100%;
    -moz-border-radius: 100%;
    border-radius: 100%;
}
/* ================= BACK TO TOP BUTTON ================== */
 .back-to-top {
    position: fixed;
    bottom: 1.5em;
    right: 1.5em;
    display: none;
    background-image:url('../images/gototop.png');
    background-repeat:no-repeat;
    background-position:center center;
    width:60px;
    height:60px;
    -webkit-border-radius: 100%;
    -moz-border-radius: 100%;
    border-radius: 100%;
    -webkit-transition:background 0.2s ease-in-out;
    -moz-transition:background 0.2s ease-in-out;
    -o-transition:background 0.2s ease-in-out;
    -ms-transition:background 0.2s ease-in-out;
    transition:background 0.2s ease-in-out;
}
/* ================= TABS ================== */
.ionTabs {
    position: relative;
}
.ionTabs__head {
    position: relative;
    display: block;
    list-style-type: none;
    margin: 0;
    padding: 0;
    z-index: 1;
}
.ionTabs__head:before, .ionTabs__head:after {
    content:"";
    display: table;
}
.ionTabs__head:after {
    clear: both;
}
.ionTabs__head {
    zoom: 1;
}
.ionTabs__tab {
    position: relative;
    display: block;
    float: left;
    list-style-type: none;
    background: none;
    margin: 0;
    padding: 0;
    cursor: pointer;
    -webkit-transition:all 0.2s ease-in-out;
    -moz-transition:all 0.2s ease-in-out;
    -o-transition:all 0.2s ease-in-out;
    -ms-transition:all 0.2s ease-in-out;
    transition:all 0.2s ease-in-out;
}
.ionTabs__tab.ionTabs__tab_state_active {
    cursor: default;
}
.ionTabs__body {
    position: relative;
    z-index: 2;
}
.ionTabs__item {
    position: relative;
    display: none;
}
.ionTabs__item.ionTabs__item_state_active {
    display: block;
}
.ionTabs__preloader {
    position: relative;
}
.ionTabs {
    margin: 0 0 30px;
}
.ionTabs__head {
}
.ionTabs__tab {
    top: 3px;
    font-size: 14px;
    line-height: 16px;
    text-transform:uppercase;
    opacity:0.7;
    padding: 20px 26px 22px
}
.ionTabs__tab:hover {
    opacity:1;
}
.ionTabs__tab.ionTabs__tab_state_active {
    top: 0;
    opacity:1;
}
.ionTabs__item {
    padding: 30px;
}
.ionTabs__preloader {
}
/* ================= UNDER CONSTRUCTION ================== */
.cs-wrapper
{
    width: 100%;
    height: 70%;
    margin: auto;
    position: absolute;
    top: 0; left: 0; bottom: 0; right: 0;
    overflow:auto;
}
#cs-main
{
    width:100%;
    height:100%;
    position:fixed;
    color:#fff;
    overflow:hidden;
}
.cs-bg
{
    background-color:#262626;
    max-width:640px;
    width:100%;
    position:relative;
    margin:auto;
    padding:20px;
}
.cs-foot .cs-icons {
    margin:0px 0px 10px 0px;
    text-align:center;
    position:relative;
    display:inline-block;
    width:100%;
}
.cs-head
{
    text-align:center;
    padding-top:20px;
    margin-bottom:25px;
    line-height:1;
}
.cs-foot
{
    max-width:640px;
    position:relative;
    width:100%;
    background-color:#1b1b1b;
    padding: 15px 0px 5px 0px;
    margin: 0 auto;
    height:auto;
    background-size: 40px 40px;
    background-image: linear-gradient(135deg, rgba(255, 255, 255, .05) 25%, transparent 25%,
							transparent 50%, rgba(255, 255, 255, .05) 50%, rgba(255, 255, 255, .05) 75%,
							transparent 75%, transparent);										
    border: none;
    -webkit-animation: animate-bg 5s linear infinite;
    -moz-animation: animate-bg 5s linear infinite;
    -o-animation: animate-bg 5s linear infinite;
    animation: animate-bg 5s linear infinite;
}
.countdown
{
    text-align:center;
    margin-bottom:10px;
}
.countdown div {
  display: inline-block;
  margin: 10px;
  font-size: 70px;
  line-height: 1;
  /* IE7 inline-block hack */
  *display: inline;
  *zoom: 1;
}
.countdown div:first-child {
  margin-left: 0;
}
.countdown div:last-child {
  margin-right: 0;
}
.countdown div span {
  display: block;
  border-top: 1px solid #da2f10;
  padding-top: 5px;
  font-size: 24px;
  font-weight: normal;
  text-align:center;
}
@keyframes animate-bg {
    from {
        background-position: 0 0;
    }
    to {
        background-position: -80px 0;
    }
}
@-webkit-keyframes animate-bg {
    from {
        background-position: 0 0;
    }
    to {
        background-position: -80px 0;
    }
}
@-moz-keyframes animate-bg {
    from {
        background-position: 0 0;
    }
    to {
        background-position: -80px 0;
    }
}
@-o-keyframes animate-bg {
    from {
        background-position: 0 0;
    }
    to {
        background-position: -80px 0;
    }
}
; TI"dependency_digest; TI"%b7bbb49c40b2e4108b7d364465f7fc97; FI"required_paths; T[I"0$root/app/assets/stylesheets/style.css.orig; FI"dependency_paths; T[{I"	path; TI"0$root/app/assets/stylesheets/style.css.orig; FI"
mtime; TI"2014-02-21T22:22:46+07:00; TI"digest; TI"%a2c7d273121619751bc1c4f5af23db0a; FI"_version; TI"%3e3098c8a5e9a7f3b984ba95ac600637; F